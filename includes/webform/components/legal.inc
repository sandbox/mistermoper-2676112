<?php
/**
 * @file
 * Webform provinces component.
 */

/**
 * Implements _webform_defaults_component().
 */
function _webform_defaults_legal() {
  return array(
    'value' => NULL,
    'legal_text' => NULL,
    'extra' => array(
      'legal_text' => NULL,
      'title_display' => TRUE,
      'private'       => FALSE,
    ),
  );
}

/**
 * Implements _webform_edit_component().
 */
function _webform_edit_legal($component) {
  $form = array();
  $form['extra']['legal_text'] = array(
    '#title' => t('Legal text'),
    '#type' => 'text_format',
    '#required' => TRUE,
    '#default_value' => !empty($component['extra']['legal_text']) ? $component['extra']['legal_text']['value'] : t('I accept the terms and conditions'),
  );

  $form['value'] = array(
    '#type' => 'checkbox',
    '#title' => t('Default value'),
    '#default_value' => $component['value'],
    '#description' => t('The default value of the field.') . ' ' . theme('webform_token_help'),
  );
  return $form;
}

/**
 * Implements _webform_render_component().
 */
function _webform_render_legal($component, $value = NULL, $filter = TRUE, $submission = NULL) {

  // Show a checkbox with a text indicating info of terms and conditions.
  $element = array(
    '#title' => $component['name'],
    '#title' => $component['extra']['legal_text']['value'],
    '#type'        => 'checkbox',
    '#default_value' => $component['value'],
    '#required'    => $component['required'],
    '#weight'      => $component['weight']
  );


  if ($component['required']) {
    $element['#attributes']['required'] = 'required';
  }

  if (isset($value[0])) {
    $element['#default_value'] = $value[0];
  }
  return $element;
}

/**
 * Implements _webform_display_component().
 */
function _webform_display_legal($component, $value, $format = 'html', $submission = array()) {
  return array(
    '#markup' => '',
  );
}
